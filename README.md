<div align="center">
<br>
<h1>SpaceXiao_Minecraft
<!-- <img src='https://gitee.com/xiaofei20625/space-xiao_minecraft/badge/star.svg?theme=dark'  alt='star'> -->
</img></a></h1>
<br>
</div> 

## 介绍
一个yunzai插件，可以配合minecraft的servertap插件在qq群/qq频道控制mc进行添加白名单，输入指令等操作

## 实现功能

- 添加白名单    
- 在qq频道执行控制台指令(暂未测试qq群聊，不确定能否正常使用)   
- 获取在线的/全部进过服务器的玩家的玩家列表 
- 向服务器发送一条消息(附带qq/频道的名字而非qq号)   
- 向服务器发送一个公告  
- 获取服务器内的消息  
- 自定义分配其他人指令权限  
- 更多指令请下载后使用 #晓mc帮助 查看

## 依赖

本yunzai插件需要在mc服务端装载servertap插件     
[servertap插件下载点我！](https://github.com/phybros/servertap/releases/latest)    
推荐将搭载本插件的机器人与minecraft服务器所在的核心防在同一台服务器下

## 安装

> Yunzai-Bot/Miao-Yunzai目录下执行

```
git clone --depth=1 https://gitee.com/xiaofeiawa/space-xiao_minecraft.git ./plugins/SpaceXiao_Minecraft/

```  

## 鸣谢

> [晓飞](https://gitee.com/xiaofei20625):仓库主人,合并,修复,优化,写新功能    
> [紅色鯡魚](https://gitee.com/smallcui):提供技术帮助，写了运行指令的api代码    

## 免责声明

1. 功能仅限内部交流与小范围使用
2. 请勿用于任何以盈利为目的的场景

## yunzai下载
本插件为yunzai插件，如要使用请先下载云崽    
另:本插件未在除Trss-Yunzai外的云崽运行过，如果有不适配的问题欢迎来本仓库进行反馈！

| 名称 | 作者 | GitHub | Gitee | 备注  | 推荐使用优先级 |
|------| ---- | ------ | ----- | ----- | ----- |
| TRSS-Yunzai | [@时雨🌌星空](../../../../TimeRainStarSky) | [☞GitHub](https://github.com/TimeRainStarSky/Yunzai) | [☞Gitee](https://gitee.com/TimeRainStarSky/Yunzai) | Yunzai 应用端，支持多账号，支持协议端：go-cqhttp、ComWeChat、GSUIDCore、ICQQ、QQ频道、微信、KOOK、Telegram、Discord | ■■■■■ |
| Miao-Yunzai | [@喵喵](https://gitee.com/yoimiya-kokomi) | [☞GitHub](https://github.com/yoimiya-kokomi/Miao-Yunzai) | [☞Gitee](https://gitee.com/yoimiya-kokomi/Miao-Yunzai) | 喵版 Yunzai | ■■■■□ |
| Yunzai-Bot | [@Le-niao](https://gitee.com/Le-niao) | [☞GitHub](https://github.com/Le-niao/Yunzai-Bot) | [☞Gitee](https://gitee.com/Le-niao/Yunzai-Bot) | 原版 Yunzai | ■□□□□ |
| Yunzai-Bot-lite | [@听语惊花](https://github.com/Nwflower) | [☞GitHub](https://github.com/Nwflower/yunzai-bot-lite) | [☞Gitee](https://gitee.com/Nwflower/yunzai-bot-lite) | 轻量版，无原神功能 | □□□□□ |