import base from './base.js';
import sanjieCfg from './Config.js';
import cfg from '../../../lib/config/config.js';

export default class Help extends base {
	constructor(e) {
		super(e);
		this.model = 'help';
	}

	static async get(e) {
		let html = new Help(e);
		return await html.getData();
	}

	async getData() {
		let helpData = sanjieCfg.getdefSet('help');
		let versionData = sanjieCfg.getdefSet('version');
		let yunzaiversion = cfg.package.version;
		const version = versionData && versionData.length && versionData[0].version;
		return {
			...this.screenData,
			saveId: 'help',
			version: version,
			yunzaiName: "Yunzai-Bot",
			yunzaiversion: yunzaiversion,
			helpData,
		};
	}
}
