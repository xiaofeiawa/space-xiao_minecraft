import base from "./base.js";

export default class Game extends base {
  constructor(e) {
    super(e);
    this.model = "show";
  }
  async get_log(myData) {
    this.model = "log";
    return {
      ...this.screenData,
      saveId: "log",
      ...myData,
    };
  }
}
