import fs from 'node:fs';
import Config from './mods/Config.js'; //引入获取配置的js
import chalk from 'chalk';
import path from "path";

if (!global.segment) { global.segment = (await import('icqq')).segment; };
let apps = {};

//定义一个版本信息的常量,获取默认文件配置文件信息
const versionData = Config.getdefSet('version');
const _path = './plugins/SpaceXiao_Minecraft';

let shibie1
try {
    shibie1 = await shibie();
}
catch {
    let dir = path.join(`${_path}/undefined/configuration.json`);
    let shibie = fs.readFileSync(dir, "utf8", (err, data) => { if (err) { console.log(err); return "error"; } return data; });
    shibie1 = JSON.parse(shibie);
    await xieru(shibie1);
    logger.info(chalk.green(`自动生成配置文件configuration.json`));
}

/**
 * 遍历获取
 */
let sum = [""];
let filepath = `${_path}/apps`;
function readdirectory(dir) {
    let files = fs.readdirSync(dir);
    files.forEach(async item => {
        let filepath1 = dir + '/' + item;
        let stat = fs.statSync(filepath1);
        if (stat.isFile()) {
        } else {
            let file = filepath1.replace(filepath, "");
            sum.push(file);
        }
    })
}
readdirectory(filepath);
/**
 * import
 */
var bian = "";
//循环写入
for (var i = 0; i < sum.length; i++) {
    bian = sum[i];
    var files = fs.readdirSync(`${_path}/apps` + bian).filter((file) => file.endsWith(".js"));
    for (let file of files) {
        let name = file.replace(".js", "");
        apps[name] = (await import('./apps' + bian + '/' + file))[name];
    }
}
//判断是否需要更新
const __dirname =
    path.resolve() + path.sep + "plugins" + path.sep + "SpaceXiao_Minecraft";
if (shibie1.version == 0) {
    logger.info(chalk.red(`检测到为第一次启动，正在执行初始化...`));
    let enroll = [];
    await Write_enroll(enroll);
    logger.info(chalk.yellow(`创建了文件enroll.json in ${__dirname}/resources/data/enroll`));
    shibie1.version = versionData[0].version;
    await xieru(shibie1);
    logger.info(chalk.green(`初始化完成，正在启动SpaceXiao_Minecraft...`));
}
//打印启动日志
logger.info(chalk.cyan('-----------(∗❛ั∀❛ั∗)✧*。--------------'));
logger.info(chalk.red(`「${versionData[0].name}」${versionData[0].version}启动成功！`));
logger.info(`~\t${chalk.yellow('作者：晓飞')}\t~`);
logger.info(chalk.cyan('------------------------------------'));

//导出
export { apps };
async function shibie() {
    let dir = path.join(`${_path}/configuration/configuration.json`);
    let shibie = fs.readFileSync(dir, "utf8", (err, data) => { if (err) { console.log(err); return "error"; } return data; });
    shibie = JSON.parse(shibie);
    return shibie;
}
async function xieru(xieru1) {
    let dir = path.join(`${_path}/configuration`, `configuration.json`);
    let new_ARR = JSON.stringify(xieru1, "", "\t");
    fs.writeFileSync(dir, new_ARR, "utf8", (err) => { console.log("写入成功", err); });
    return;
}
async function Write_enroll(wupin) {
    let dir = path.join(`${_path}/resources/data/enroll`, `enroll.json`);
    let new_ARR = JSON.stringify(wupin, '', '\t');
    fs.writeFileSync(dir, new_ARR, 'utf8', (err) => {
        console.log('写入成功', err);
    });
    return;
}
