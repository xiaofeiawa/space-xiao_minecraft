//插件加载
import Help from '../../mods/help.js';
import puppeteer from '../../../../lib/puppeteer/puppeteer.js';
import md5 from 'md5';
let helpData = {
    md5: '',
    img: '',
};

export class help extends plugin {
    constructor() {
        /**
         * ServerTap api网址
         * http://localhost:55590/swagger
         */
        super({
            name: 'help',
            dsc: '帮助模块',
            event: 'message',
            priority: 600,
            rule: [
                {
                    reg: '^(#|/)晓mc帮助$',
                    fnc: 'xiaohelp',
                },
            ],
        });
    }
    async xiaohelp(e) {
        let data = await Help.get(e);
        if (!data) return;
        let img = await this.cache(data);
        await e.reply(img);
    }
    async cache(data) {
        let tmp = md5(JSON.stringify(data));
        if (helpData.md5 == tmp) return helpData.img;
        helpData.img = await puppeteer.screenshot('help', data);
        helpData.md5 = tmp;
        return helpData.img;
    }
}