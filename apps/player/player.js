//插件加载
import { instruct_cmd, ping, Permissions } from '../all/all.js'
import fetch from 'node-fetch'//导入fetch否则会用不了报错
import config from '../../mods/Config.js';
import Show from '../../mods/show.js';
import puppeteer from '../../../../lib/puppeteer/puppeteer.js';
export class player extends plugin {
    constructor() {
        /**
         * ServerTap api网址
         * http://localhost:55590/swagger
         */
        super({
            name: 'player',
            dsc: '玩家模块',
            event: 'message',
            priority: 600,
            rule: [
                {
                    reg: '^(#|/)获取(在线玩家列表|全部玩家列表|白名单列表).*$',
                    fnc: 'lookplayer',
                },
                {
                    reg: '^(#|/)发言.*$',
                    fnc: 'say',
                },
                {
                    reg: '^(#|/)公告.*$',
                    fnc: 'title',
                },
                {
                    reg: '^(#|/)查看服务器tps$',
                    fnc: 'tps',
                }
            ],
        });
        this.mcConfigData = config.getConfig('mc');
    }
    async tps(e) {
        let ok = await ping();
        if (!ok) {
            e.reply("服务器未开启！");
            return;
        }
        //http://localhost:55590/v1/server
        const ip = `${this.mcConfigData.ip}:${this.mcConfigData.port}`
        const response = await fetch(`http://${ip}/v1/server`);
        const data = await response.json();
        e.reply(`当前tps:${data.tps}`);
        return;
    }
    //e.author.username 用户name
    async say(e) {
        let ok = await ping();
        if (!ok) {
            e.reply("服务器未开启！");
            return;
        }
        let thing = e.msg.replace('#', '');
        thing = thing.replace('/', '');
        thing = thing.replace('发言', '');
        let code = thing.split('*');
        let gomap = code[0]
        let player = e.author.username
        let wenan = `${player} 说:${gomap}`
        let say = `say ${wenan}`
        await instruct_cmd(say)
        e.reply(`发送完成`)
        return
    }
    async title(e) {
        let ok = await ping();
        if (!ok) {
            e.reply("服务器未开启！");
            return;
        }
        let qq = e.user_id
        let yanzheng = await Permissions(qq, 2, e)
        if (!yanzheng) return;
        let thing = e.msg.replace('#', '');
        thing = thing.replace('/', '');
        thing = thing.replace('公告', '');
        let code = thing.split('*');
        const wenan = code[0]
        let color = code[1]
        switch (color) {
            case "黄色": color = "yellow"; break;
            case "绿色": color = "green"; break;
            case "青色": color = "aqua"; break;
            case "黑色": color = "black"; break;
            case "蓝色": color = "blue"; break;
            case "金色": color = "gold"; break;
            case "灰色": color = "gray"; break;
            case "白色": color = "white"; break;
            default: color = "red"; break;
        }
        let say = `title @a title {"text":"${wenan}","color":"${color}"}`
        await instruct_cmd(say)
        e.reply(`发送完成`)
        return
    }
    async lookplayer(e) {
        let ok = await ping();
        if (!ok) {
            e.reply("服务器未开启！");
            return;
        }
        let thing = e.msg.replace('#', '');
        thing = thing.replace('/', '');
        thing = thing.replace('获取', '');
        let code = thing.split('*');
        let gomap = code[0]
        let num = code[1]
        if (num == undefined || num == 0 || num < 0) num = 0
        else num -= 1
        const ip = `${this.mcConfigData.ip}:${this.mcConfigData.port}`
        if (gomap == `在线玩家列表`) {
            //http://localhost:55590/v1/players
            const response = await fetch(`http://${ip}/v1/players`);
            const data = await response.json()
            let wenan = []
            wenan.push(`在线玩家列表:`)
            for (let i in data) {
                if (data[i].displayName != undefined) {
                    const health = Math.trunc(data[i].health);
                    const hunger = Math.trunc(data[i].hunger);
                    wenan.push(`name:${data[i].displayName} 血量:${health} 饱食度:${hunger}`)
                }
            }
            if (wenan == `在线玩家列表:`) wenan = [`似乎没有人在线呢~`]
            let img = await get_player_img(e, num, wenan)
            e.reply(img);
            return
        } else if (gomap == `全部玩家列表`) {
            //http://localhost:55590/v1/players/all
            const response = await fetch(`http://${ip}/v1/players/all`);
            const data = await response.json()
            let wenan = []
            wenan.push(`全部玩家列表:`)
            for (let i in data) {
                if (data[i].name != undefined) {
                    //处理上次登入时间
                    const day = data[i].lastPlayed
                    const date = new Date(day);  // 参数需要毫秒数，所以这里将秒数乘于 1000
                    const Y = date.getFullYear() + '-';
                    const M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
                    const D = date.getDate() + ' ';
                    const h = date.getHours() + ':';
                    const m = date.getMinutes();
                    wenan.push(`name:${data[i].name} 上次入服时间:${Y}${M}${D}${h}${m}`)
                }
            }
            let img = await get_player_img(e, num, wenan)
            e.reply(img);
            return
        } else if (gomap == `白名单列表`) {
            const response = await fetch(`http://${ip}/v1/server/whitelist`);
            const data = await response.json()
            let wenan = []
            wenan.push(`白名单列表:`)
            for (let i in data) {
                if (data[i].name != undefined) {
                    wenan.push(data[i].name)
                }
            }
            let img = await get_player_img(e, num, wenan)
            e.reply(img);
            return
        }
        e.reply(`似乎...报错了呢qwq...`)
        return
    }
}
export async function get_player_img(e, num, data) {
    const num1 = num * 18;
    const num2 = num1 + 18;
    const nummix = Math.ceil(data.length / 18);
    const numnow = num += 1;
    const data2 = data.slice(num1, num2);
    const new_data = {
        log: data2,
        numnow,
        nummix
    }
    if (numnow > nummix) {
        e.reply(`貌似没有这页呢！`)
        return
    }
    const data1 = await new Show(e).get_log(new_data);
    return await puppeteer.screenshot('log', {
        ...data1,
    });
}

