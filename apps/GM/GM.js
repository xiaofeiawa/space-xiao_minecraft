//插件加载
import { Write_Permissions, Read_Permissions } from '../all/all.js'
import Config from '../../mods/Config.js'; //引入获取配置的js
export class GM extends plugin {
    constructor() {
        super({
            name: 'GM',
            dsc: 'GM模块',
            event: 'message',
            priority: 600,
            rule: [
                {
                    reg: '^(#|/)添加权限.*',
                    fnc: 'add_Permissions',
                },
                {
                    reg: '^(#|/)查看权限列表$',
                    fnc: 'look_Permissions',
                },
                {
                    reg: '^(#|/)撤销权限.*',
                    fnc: 'quash_Permissions',
                },
                {
                    reg: '^(#|/)设置ip.*',
                    fnc: 'add_ip',
                },
            ],
        });
    }
    async add_ip(e) {
        if (!e.isMaster) return;
        let thing = e.msg.replace('#', '');
        thing = thing.replace('/', '');
        thing = thing.replace('设置ip', '');
        const code = thing.split('*');
        let ip = code[0]
        let port = code[1]
        switch (code[0]) {
            case "本机":
                ip = "localhost"
                break;
            case "默认":
                ip = "localhost"
                port = 4567
                break;
        }
        if (ip == undefined || port == undefined) {
            e.reply(`请输入正确的ip或者端口！`)
            return
        }
        let look = await Config.getConfig("mc")
        look.ip = ip
        look.port = port
        await Config.setConfig("mc", look)
        e.reply(`修改完成!新的ip地址为:${ip}:${port}`)
        return

    }
    async quash_Permissions(e) {
        if (!e.isMaster) return;
        let thing = e.msg.replace('#', '');
        thing = thing.replace('/', '');
        thing = thing.replace('撤销权限', '');
        const code = thing.split('*');
        const quash = code[0];
        const isat = e.message.some((item) => item.type === 'at');
        if (!isat) return;
        const atItem = e.message.filter((item) => item.type === 'at'); //获取at信息
        const name = atItem[0].qq; //对方qq
        let look;
        try {
            look = await Read_Permissions();
        }
        catch {
            const bug = [];
            await Write_Permissions(bug);
            e.reply(`当前没有权限文件，已自动生成文件`);
            e.reply(wenan);
            return;
        }
        if (quash == `全部`) {
            for (let i in look) {
                if (name == look[i].qq) {
                    const neme = look[i].qq
                    look = look.filter((item) => item.qq != name);
                    await Write_Permissions(look);
                    let msg = [segment.at(neme)];
                    msg.push("你的全部权限被撤销了");
                    e.reply(msg);
                    return;
                }
            }
            e.reply("该用户无任何权限！");
            return;
        }
        for (let i in look) {
            if (name == look[i].qq) {
                for (let x in look[i].Permissions) {
                    if (quash == look[i].Permissions[x]) {
                        let wenan = ``
                        const neme = look[i].qq
                        switch (look[i].Permissions[x]) {
                            case "1": wenan = `开启消息获取权限`; break;
                            case "2": wenan = `公告权限`; break;
                            case "3": wenan = `添加白名单权限`; break;
                            case "4": wenan = `指令权限`; break;
                            case "5": wenan = `插件更新权限`; break;
                        }
                        look[i].Permissions.splice(x, 1)
                        await Write_Permissions(look);
                        if (look[i].Permissions.length == 0) {
                            look = look.filter((item) => item.qq != name);
                            await Write_Permissions(look);
                        }
                        let msg = [segment.at(neme)];
                        msg.push("你的" + wenan + "被撤销了");
                        e.reply(msg);
                        return;
                    }
                }
            }
        }
        e.reply("该用户无该权限！");
        return;
    }
    async look_Permissions(e) {
        if (!e.isMaster) return;
        let wenan = `权限列表:\n开启消息获取--1，公告--2，添加白名单--3，指令--4，插件更新--5`;
        let look;
        try {
            look = await Read_Permissions();
        }
        catch {
            const bug = [];
            await Write_Permissions(bug);
            e.reply(`当前没有权限文件，已自动生成文件`);
            e.reply(wenan);
            return;
        }
        for (let i in look) {
            let Permissions = ``;
            for (let x in look[i].Permissions) {
                Permissions += `\n`;
                switch (look[i].Permissions[x]) {
                    case "1": Permissions += `开启消息获取权限`; break;
                    case "2": Permissions += `公告权限`; break;
                    case "3": Permissions += `添加白名单权限`; break;
                    case "4": Permissions += `指令权限`; break;
                    case "5": Permissions += `插件更新权限`; break;
                }
            }
            wenan += `\n账号:\n${look[i].qq}\n权限:${Permissions}`;
        }
        e.reply(wenan);
        return;
    }
    async add_Permissions(e) {
        if (!e.isMaster) return;
        let thing = e.msg.replace('#', '');
        thing = thing.replace('/', '');
        thing = thing.replace('添加权限', '');
        const code = thing.split('*');
        const Permissions = code[0];
        const isat = e.message.some((item) => item.type === 'at');
        if (!isat) return;
        const atItem = e.message.filter((item) => item.type === 'at'); //获取at信息
        const name = atItem[0].qq; //对方qq
        //先检查有没有json文件，没有创建一个
        let look;
        try {
            look = await Read_Permissions();
        }
        catch {
            const bug = [];
            await Write_Permissions(bug);
        }
        //检查该玩家有没有表格
        for (let i in look) {
            if (name == look[i].qq) {
                for (let x in look[i].Permissions) {
                    if (Permissions == look[i].Permissions[x]) {
                        e.reply(`他已经有这个权限了！`);
                        return;
                    }
                }
                look[i].Permissions.push(Permissions);
                await Write_Permissions(look);
                e.reply(`添加成功！`);
                return;
            }
        }
        //没有创建一个
        const archives = {
            qq: name,
            Permissions: [Permissions]
        }
        look.push(archives);
        await Write_Permissions(look);
        e.reply(`添加成功！`);
        return;
    }
}