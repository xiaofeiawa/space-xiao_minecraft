import fs from "fs";
import path from "path";
import config from '../../mods/Config.js';
import fetch from 'node-fetch';//导入fetch否则会用不了报错

/**
* ServerTap api网址
* http://localhost:55590/swagger
*/
//插件根目录
const __dirname =
    path.resolve() + path.sep + "plugins" + path.sep + "SpaceXiao_Minecraft";
// 文件存放路径
export const __PATH = {
    //玩家数据库
    enroll_path: path.join(__dirname, "/resources/data/enroll"),
    Permissions: path.join(__dirname, "/resources/data/Permissions"),
    data: path.join(__dirname, "/resources/data/data"),
    list: path.join(__dirname, "/resources/data/list"),
};
let mcConfigData = config.getConfig('mc');
//处理消息
export class all extends plugin {
    constructor() {
        super({
            name: "all",
            dsc: "数据模块",
            event: "message",
            priority: 800,
            rule: [],
        });
    }
}
//写入白名单信息,第二个参数是一个JavaScript对象
export function Write_enroll(enroll) {
    let dir = path.join(__PATH.enroll_path, `enroll.json`);
    let new_ARR = JSON.stringify(enroll, "", "\t");
    fs.writeFileSync(dir, new_ARR, "utf8", (err) => {
        console.log("写入成功", err);
    });
    return;
}
//写入白名单信息,第二个参数是一个JavaScript对象
export function Read_enroll() {
    let dir = path.join(`${__PATH.enroll_path}/enroll.json`);
    let enroll = fs.readFileSync(dir, "utf8", (err, data) => {
        if (err) {
            console.log(err);
            return "error";
        }
        return data;
    });
    //将字符串数据转变成数组格式
    enroll = JSON.parse(enroll);
    return enroll;
}
//执行输入进去的指令
export async function instruct_cmd(command, time = 1) {
    const ip = `${mcConfigData.ip}:${mcConfigData.port}`
    const urlSearchParams = new URLSearchParams();
    urlSearchParams.append('command', command);
    urlSearchParams.append('time', time);
    const response = await fetch(`http://${ip}/v1/server/exec`, {
        'method': 'POST',
        'accept': '*/*',
        'Content-Type': 'application/x-www-form-urlencoded',
        'body': urlSearchParams,
    },
    )
    const data = await response.text();
    console.log(data);
    return data
}

//查看白名单是否添加成功
export async function look_whitelist(player) {
    //http://localhost:55590/v1/server/whitelist
    const ip = `${mcConfigData.ip}:${mcConfigData.port}`
    const response = await fetch(`http://${ip}/v1/server/whitelist`);
    const data = await response.json()
    for (let i in data) { if (data[i].name == player) return false }
    return true
}
export async function sleep(time) { return new Promise((resolve) => { setTimeout(resolve, time); }) }
export async function ping() {
    //http://localhost:55590/v1/ping
    try {
        const ip = `${mcConfigData.ip}:${mcConfigData.port}`
        const response = await fetch(`http://${ip}/v1/ping`);
        if(response == "pong") return true
        else return false
    }
    catch {
        return false
    }
}
export function Write_Permissions(Permissions) {
    let dir = path.join(__PATH.Permissions, `Permissions.json`);
    let new_ARR = JSON.stringify(Permissions, "", "\t");
    fs.writeFileSync(dir, new_ARR, "utf8", (err) => {
        console.log("写入成功", err);
    });
    return;
}
//写入权限信息,第二个参数是一个JavaScript对象
export function Read_Permissions() {
    let dir = path.join(`${__PATH.Permissions}/Permissions.json`);
    let Permissions = fs.readFileSync(dir, "utf8", (err, data) => {
        if (err) {
            console.log(err);
            return "error";
        }
        return data;
    });
    //将字符串数据转变成数组格式
    Permissions = JSON.parse(Permissions);
    return Permissions;
}
export async function Write_data(today, data) {
    let dir = path.join(__PATH.data, `${today}.json`);
    let new_ARR = JSON.stringify(data, "", "\t");
    fs.writeFileSync(dir, new_ARR, "utf8", (err) => {
        console.log("写入成功", err);
    });
    return;
}
//写入数据信息,第二个参数是一个JavaScript对象
export async function Read_data(today) {
    let dir = path.join(`${__PATH.data}/${today}.json`);
    let data = fs.readFileSync(dir, "utf8", (err, data) => {
        if (err) {
            console.log(err);
            return "error";
        }
        return data;
    });
    //将字符串数据转变成数组格式
    data = JSON.parse(data);
    return data;
}
export function Read_list() {
    let dir = path.join(`${__PATH.list}/list.json`);
    let list = fs.readFileSync(dir, "utf8", (err, data) => {
        if (err) {
            console.log(err);
            return "error";
        }
        return data;
    });
    //将字符串数据转变成数组格式
    list = JSON.parse(list);
    return list;
}
export async function Permissions(qq, num, e) {
    if (!e.isMaster) {
    } else return true;
    let look;
    try {
        look = await Read_Permissions();
    }
    catch {
        const bug = [];
        await Write_Permissions(bug);
        e.reply(`当前没有权限文件，已自动生成文件`);
        e.reply(wenan);
        return;
    }
    for (let i in look) {
        if (qq == look[i].qq) {
            for (let x in look[i].Permissions) {
                if (num == look[i].Permissions[x]) {
                    return true;
                }
            }
        }
    }
    return false;
}