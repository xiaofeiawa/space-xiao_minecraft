//插件加载
import { Write_enroll, Read_enroll, instruct_cmd, look_whitelist, sleep, ping, Permissions } from '../all/all.js'
import config from '../../mods/Config.js';
export class whitelist extends plugin {
    constructor() {
        /**
         * ServerTap api网址
         * http://localhost:55590/swagger
         */
        super({
            name: 'whitelist',
            dsc: '白名单模块',
            event: 'message',
            priority: 600,
            rule: [
                {
                    reg: '^(#|/)指令.*$',
                    fnc: 'directives',
                },
                {
                    reg: '^(#|/)申请白名单.*$',
                    fnc: 'enroll',
                },
                {
                    reg: '^(#|/)添加白名单.*$',
                    fnc: 'addenroll',
                },
            ],
        });
        this.mcConfigData = config.getConfig('mc');
    }
    async addenroll(e) {
        let qq = e.user_id
        let yanzheng = await Permissions(qq, 3, e)
        if (!yanzheng) return;
        let thing = e.msg.replace('#', '');
        thing = thing.replace('/', '');
        thing = thing.replace('添加白名单', '');
        let code = thing.split('*');
        let list = await Read_enroll()
        for (let v in code) {
            for (let i in list) {
                if (list[i].name == code[v]) {
                    e.reply(`不可以重复添加哦！`)
                    return
                }
            }
            let yes = await look_whitelist(code[v])
            if (!yes) {
                e.reply(`有玩家已经在白名单中了！`)
                return
            }
            let new_list = {
                name: code[v]
            }
            list.push(new_list)
        }
        await Write_enroll(list)
        e.reply(`添加成功`)
        return
    }
    async directives(e) {
        let qq = e.user_id
        let yanzheng = await Permissions(qq, 4, e)
        if (!yanzheng) return;
        let ok = await ping();
        if (!ok) {
            e.reply("服务器未开启！");
            return;
        }
        let thing = e.msg.replace('#', '');
        thing = thing.replace('/', '');
        thing = thing.replace('指令', '');
        let code = thing.split('*');
        code.map(cmd => instruct_cmd(cmd)
            .then(say => say && e.reply(say)));
        try { e.reply(say); return }
        catch { e.reply(`操作完成`); return }
    }
    async enroll(e) {
        let thing = e.msg.replace('#', '');
        thing = thing.replace('/', '');
        thing = thing.replace('申请白名单', '');
        let code = thing.split('*');
        let gomap = code[0], list = await Read_enroll(), i = 0
        if (gomap == ``) return
        for (i in list) {
            if (list[i].name == gomap) {
                let directives = `whitelist add ${list[i].name}`
                await instruct_cmd(directives)
                e.reply(`指令已执行，正在验证是否成功...[8s]`)
                //设置5s延迟防止白名单获取不到最新
                await sleep(8000)
                console.log(list[i].name);
                let yes = await look_whitelist(list[i].name)
                console.log(yes);
                if (!yes) {
                    list = list.filter((item) => item.name != gomap);
                    await Write_enroll(list)
                    e.reply(`白名单添加成功！\n欢迎感谢你对服务器的支持！`)
                    return
                } else {
                    e.reply(`白名单添加错误！\n请先尝试进入游戏刷新一次uuid再申请白名单`)
                    return
                }
            }
        }
        e.reply(`你申请的名字貌似还没通过审核哦~\n请检查申请的游戏名是否正确\n请先进行白名单申请！`)
        return
    }
}
